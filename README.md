# No-Fly Zone

This is a small first-person shooter where the goal is to stay alive and shoot down quadcopters that have become sentient and are trying to kill you. I wrote this game to provide a learning experience for anyone wanting to learn about game development.

The main development philosophy behind this project was to keep everything as simple as possible. To keep things simple I've intentionally tried to avoid overengineering.

This project uses OpenGL 3.2 Core. This GL version is widely supported by older and more modern hardware and prevents the use of deprecated functionality. While the source code is heavily commented, it doesn't spend a lot of time explaining how OpenGL works or the particular facilities I've chosen to use. A good starting point is Luke Benstead's "Beginning OpenGL Game Programming, 2nd Edition", although it is now slightly out of date. A few of my GitLab projects go into more detail about things like instanced rendering, for example, which I make heavy use of in this project.

Cross-platform. Tested in Linux Mint (3.19.0-32-generic) and Windows 10.

## Compiling and Running

Executables are included in the **mingw32** and **linux-gcc** folders.

Separate project files for Windows and Linux are included. In the future, I may switch to CMake, but I do the majority of my game development using the excellent Code::Blocks IDE. Download the IDE [here](http://codeblocks.org/downloads/26).

If you want to make changes to the game, simply open up the project file specific to your system. Pressing F9 will build and run. Ctrl-F11 will do a full rebuild.

## Folder Structure

**ico**: Contains a "No Drone Zone" icon based on the image provided by the FAA.

**linux-gcc**: Contains the CodeBlocks project for compiling No-Fly Zone in Linux. Executables are also provided.

**mesh**: Contains the Wavefront .OBJ files and .BLEND edits I used in this project.

**mingw32**: Contains the CodeBlocks project for compiling No-Fly Zone in Windows. Executables are also provided.

**png**: Contains the .PNG images I use for this project as well as the .XCF edits.

**shaders**: Contains the .VERT and .FRAG GLSL shader source code used in this project.

**src**: Contains all of the source code for this project, including 3rd party source code (GLM, GLFW, etc.) in one of the sub-folders.

**wav**: Contains all of the .WAV sound effects used in No-Fly Zone.

## Attributions

Not all of the assets used in this project are my own work. Those that aren't are used under appropriate licenses, as described in ATTRIBUTIONS.txt. All other work and assets are my own.

## Contact

If you have any questions or bug reports, please feel free to drop me a line at geoff.nagy@gmail.com.